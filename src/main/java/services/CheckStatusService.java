package services;

import entities.Customer;

public interface CheckStatusService {
    String checkStatus(Customer customer);
    Customer getCustomerDetails(String mobileNumber);
    String updateStatus(String status,String pan);
}
