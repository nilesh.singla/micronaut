package services;

import entities.Customer;

public interface AddCustomerService {
    String addCustomer(Customer customer);
}
