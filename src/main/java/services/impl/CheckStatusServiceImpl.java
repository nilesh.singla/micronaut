package services.impl;

import dao.CustomerDao;
import entities.Customer;
import entities.PANDetails;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.*;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.netty.DefaultHttpClient;
import jakarta.inject.Inject;
import org.json.JSONObject;
import services.CheckStatusService;

@Bean
public class CheckStatusServiceImpl implements CheckStatusService {

    @Inject
    CustomerDao customerDao=new CustomerDao();

    @Value("${pan-authentication.url}")
    private String panAuthURL;

    @Value("${pan-authentication.authToken_key}")
    private String panAuthTokenKey;

    @Value("${pan-authentication.authToken_value}")
    private String panAuthTokenValue;

    @Inject
    @Client("/")
    private io.micronaut.http.client.HttpClient httpClient = new DefaultHttpClient();

    @Override
    public String checkStatus(Customer customer) {
        String verificationStatus="";
        try {
            PANDetails panDetails = new PANDetails(customer);
           String result =  httpClient.toBlocking().retrieve(
                    HttpRequest.POST(panAuthURL, panDetails)
                            .header(panAuthTokenKey, panAuthTokenValue));

            JSONObject finalResult=new JSONObject(result);
            verificationStatus=finalResult.getJSONObject("result").get("status").toString();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return verificationStatus;

    }

    @Override
    public Customer getCustomerDetails(String mobileNumber){return customerDao.fetchCustomer(mobileNumber);}

    @Override
    public String updateStatus(String status,String pan){return customerDao.updateStatus(status,pan);}

}
