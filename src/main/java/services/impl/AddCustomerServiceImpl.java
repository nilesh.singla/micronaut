package services.impl;

import dao.CustomerDao;
import entities.Customer;
import jakarta.inject.Inject;
import services.AddCustomerService;

public class AddCustomerServiceImpl implements AddCustomerService {

    @Inject
    CustomerDao customerDao;
    @Override
    public String addCustomer(Customer customer) {
        return customerDao.addCustomer(customer);
    }
}
