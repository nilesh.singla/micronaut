package entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


public class PANDetails {
    private String pan;
    private String name;
    private String dob;
    private String consent;

    public PANDetails(Customer customer) {
        this.pan = customer.getPAN();
        this.name = customer.getName();
        this.dob = customer.getDob();
        this.consent = "Y";
    }

    public String getPan() {
        return pan;
    }

    public String getName() {
        return name;
    }

    public String getDob() {
        return dob;
    }

    public String getConsent() {
        return consent;
    }
}
