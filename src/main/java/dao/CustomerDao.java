package dao;

import entities.Customer;
import io.micronaut.context.annotation.Bean;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.jdbi.v3.core.Jdbi;

import javax.sql.DataSource;
import javax.transaction.Transactional;

@Bean
public class CustomerDao {

    @Inject
    DataSource dataSource;
    int rowsUpdated;

    @Transactional
    public Customer fetchCustomer(String mobileNumber){
         Jdbi jdbi= Jdbi.create(dataSource);
            return jdbi.withHandle(handle -> handle.createQuery("select * from customers where mobileNumber=:mobilenumber").bind("mobilenumber",mobileNumber).mapToBean(Customer.class).first());
    }
    @Transactional
    public String addCustomer(Customer customer){
         Jdbi jdbi= Jdbi.create(dataSource);
            rowsUpdated=0;
            jdbi.useHandle(handle ->
                    rowsUpdated=handle.execute("INSERT INTO customers(name,dob,PAN,mobileNumber,email,verification_status) VALUES(?,?,?,?,?,?)",
                            customer.getName(),customer.getDob(),customer.getPAN(),customer.getMobileNumber(),customer.getEmail(),"Not Verified!"));
            if(rowsUpdated==1)
                return "Customer Added!";
            return "Customer could not be added!";
    }
    @Transactional
    public String updateStatus(String status,String pan){
         Jdbi jdbi= Jdbi.create(dataSource);
            rowsUpdated=0;
            jdbi.useHandle(handle ->
                    rowsUpdated=handle.execute("UPDATE customers SET verification_status=? where pan=?",status,pan));
            if(rowsUpdated==1)
                return "Customer Updated!";
            return "Customer could not be updated!";
    }
}
