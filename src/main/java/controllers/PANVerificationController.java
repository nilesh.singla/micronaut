package controllers;

import entities.Customer;
import io.micronaut.http.annotation.*;
import jakarta.inject.Inject;
import services.CheckStatusService;


@Controller("/checkstatus/{mobileNumber}")
public class PANVerificationController {
    @Inject
    CheckStatusService checkStatusService;

    @Inject
    Customer customer;

    @Get
    public Customer verifyPAN(@QueryValue String mobileNumber) {
        if(mobileNumber==null)
            return null;
        customer = checkStatusService.getCustomerDetails(mobileNumber);
        if(customer!=null){
            String status=checkStatusService.checkStatus(customer);
            checkStatusService.updateStatus(status,customer.getPAN());
            customer = checkStatusService.getCustomerDetails(mobileNumber);
        }
        return customer;
    }
}
