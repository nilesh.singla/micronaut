package controllers;

import entities.Customer;
import io.micronaut.http.annotation.*;
import jakarta.inject.Inject;
import services.AddCustomerService;

@Controller("/addcustomer")
public class AddCustomerController {
    @Inject
    AddCustomerService addCustomerService;
    @Post
    public String addCustomer(@Body Customer customer) {
        if(customer!=null)
            return addCustomerService.addCustomer(customer);
        return "Please enter correct details!";
    }
}